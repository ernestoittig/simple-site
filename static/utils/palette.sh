#!/bin/sh
PALETTEID=dos

if [[ $# -eq 1 ]]; then
	PALETTEID=$1;
fi

case $PALETTEID in
	ega | vga | dos)
	echo -en "\e]P0000000\e]P1aa0000\e]P200aa00\e]P3aa5500\e]P40000aa\e]P5aa00aa\e]P600aaaa\e]P7aaaaaa\e]P8555555\e]P9ff5555\e]PA55ff55\e]Pbffff55\e]Pc5555ff\e]Pdff55ff\e]Pe55ffff\e]Pfffffff\007"
	clear;;
	tango)
	echo -en "\e]P02e3436\e]P1cc0000\e]P24e9a06\e]P3c4a000\e]P43465a4\e]P575507b\e]P607c7ca\e]P7d3d7cf\e]P8888a85\e]P9ef2929\e]PA8ae234\e]Pbfce94f\e]Pc729fcf\e]Pdad7fa8\e]Pe63e9e9\e]Pfffffff\007"
	clear;;
	mac | macos | macosx | osx)
	echo -en "\e]P0000000\e]P1c23621\e]P225bc24\e]P3adad27\e]P4492ee1\e]P5d338d3\e]P633bbc8\e]P7cccbcd\e]P8818383\e]P9fc391f\e]PA31e722\e]Pbeaec23\e]Pc5833ff\e]Pdf935f8\e]Pe14f0f0\e]Pfe9ebeb\007"
	clear;;
	windows | windows10 | win10)
	echo -en "\e]P00c0c0c\e]P1c50f1f\e]P213a10e\e]P3c19c00\e]P40037da\e]P5881798\e]P63a96dd\e]P7cccccc\e]P8767676\e]P9e74856\e]PA16c60c\e]Pbf9f1a5\e]Pc3b78ff\e]Pdb4009e\e]Pe61d6d6\e]Pff2f2f2\007"
	clear;;
	*)
	echo "Unknown palette ID";;
esac

