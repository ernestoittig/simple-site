.header {
  background: #221f31;
  color: #ea9182;
}

@media screen {
	
	.header_image {
		padding-top: 12.5%;
		background-image: url("doom_header.gif");
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center;	
		image-rendering: pixelated;
		image-rendering: crisp-edges;
	}
	
	.header_text {
	}

}

