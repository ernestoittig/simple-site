% rnd's software
% /dev/urandom
% august 2021

This is a collection of different programs I wrote over the years under
different names.

* [putin](https://github.com/usrshare/putin): a small command-line tool for
  corrupting ROM files for old video games.

* [snescityeditor](https://github.com/usrshare/snescityeditor): an editor that
  modifies SRAM data for the SNES version of Sim City (as well as the [NES
  prototype](https://hiddenpalace.org/SimCity_(NES_prototype)!) to let you build
  on whichever maps you want. (Has a graphical interface and Windows builds, see
  [the releases section](https://github.com/usrshare/snescityeditor/releases).)

* [snowbot](https://github.com/usrshare/snowbot): a tiny IRC bot that I still
  run on some servers. Doesn't support SSL, though, so a tool like `stunnel` is
  required.

* [szsol](https://github.com/slashdevslashurandom/szsol): an ncurses-based
  reimplementation of solitaire games from Zachtronics' "SHENZHEN I/O",
  "Exapunks" and "Eliza".

* [AMDGPU demo fork](https://gitlab.com/dev_urandom/color-demo-app): a modified
  version of AMD's color demo that lets you more precisely change how colors are
  converted. I use it in order to make colors look good on my rather cheap TV.

* [Neocities uploader
  script](https://gitlab.com/dev_urandom/neocities-uploader): a bash script that
  checks which files of a Neocities website needs changing, and uploads these
  using the website's API.

* [P8SCII encoder](https://gitlab.com/dev_urandom/p8scii-encoder): a tool to
  convert arbitrary data into text strings readable by PICO-8. Useful for
  working with PCM samples. (Windows builds available [in the releases
  section](https://gitlab.com/dev_urandom/p8scii-encoder/-/releases).)
