% rnd's website
% /dev/urandom
% date unknown

<style>
.header h3, .titlesep {
	display: none;
}
</style>

# Hello and welcome!

> %warning%
> This is yet another of those "under construction" messages you'd see on
> pre-Web 2.0 websites.

Welcome to my little website! I'm running it mostly as an experiment in static
website generation, but also to host some content that others might find
interesting.

* [about me](about_me.html)
  
  basic info about who I am and what I do.

* [lipu sona pona - a toki pona course](tokipona/)
  
  a short course that will teach you a minimalist language of 120 words in a
  baker's dozen of pages, plus a bunch of additional materials. 

  [Deutsch](tokipona/de_index.html) / [Polski](tokipona/pl_index.html) / 
  [русский](tokipona/ru_index.html)

* [politics](politics/)
  
  a few links and texts about my opinions on Russian and worldwide politics.

* [blog thingy](blog/)
  
  typically where I put my random thoughts or information that I'd like to
  remember.

* [text pages](text/)
  
  so far only a test, but I made this as an experiment in imitating FAQ-like
  text pages.

* [rnd's games](games.html)
  
  games that I have made. So far there's only one, and it's a port of the card
  game "Donsol".

* [rnd's software](software.html)

  assorted open-source tools I wrote over the years.

* [pixel art](pixel_art.html)
  
  assorted pixel arts that I made.

* [pixel art fonts](pixel_art_fonts.html)
  
  and different bitmap fonts that could work well in games.

* [DOOM](doom/)

  my page about the classic 1993/1994 first-person shooter.

* [riichi mahjong](riichi_mahjong.html) **NEW**
  
  a japanese board game I occasionally like to play
  
---

* **[cool websites](cool_websites.html)**
  
  a sheet of 88x31 banners (some drawn by me) for websites that I think are
  cool!

* [random links](links.html)
  
  a more organized list of links to pages that I found interesting or want to
  remember.

---

* [ascii table](ascii.html)

  i saw complaints that a lot of ASCII tables on the internet are badly
	designed, and tried to make one that looks better.

* [building a static website](static.html)

* [website faq](faq.html)

* [my banners](banners.html)
  
  in case you think my website is cool for some reason?

* [license](license.html)
  
  how you can use my website's contents or the source code used to build it

