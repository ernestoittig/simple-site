% lipu sona pona (curso de toki pona)
% /dev/urandom
% febrero 2022

Esta es una serie de páginas que contienen un intento de curso educacional no
oficial sobre el *toki pona*, una lengua construida originalmente diseñada en
2001 y luego gradualmente revisada a través de los años por Sonja Lang.

El idioma fue diseñado sobre las ideas del diseño minimalista y de
simplificar los pensamientos de uno descomponiendo ideas complejas a sus
componentes básicos. Solo usa 120 "palabras oficiales" (con la excepción de
algunas palabras adicionales usadas a veces por la comunidad), tiene una
gramática increíblemente simple y usa pocos sonidos que no son fáciles de
confundir.

Por esto, el idioma se considera increíblemente fácil de aprender. Algunas
personas alegan que pueden leerlo después de solo días y logran fluidez dentro
de una semana o dos.

Sin embargo, con esa simplicidad también vienen limitaciones. Muchas palabras
tienen varios significados, y algunas frases u oraciones son ambiguas sin
contexto. Para expresar muchos conceptos e ideas en toki pona uno tiene que
inventar nuevas frases o reformularlas completamente (que es en sí parte de la
idea del idioma).

Hablando de contexto, el toki pona es una lengua muy sensible al contexto.
Gente diferente puede puede describir las mismas ideas básicas de maneras
completamente diferentes. Esto es también parte de la idea del idioma. Incluso
algunas de las reglas del idioma son interpretadas de manera diferente por
diferentes personas, dependiendo de su lengua materna o de sus opiniones de
cuál es la mejor forma de comunicar algo.

Además, el toki pona fue diseñado para ser fácil de usar independientemente de
la lengua materna de uno. Los sonidos y la estructura de las sílabas usados en
toki pona son distintos los unos de los otros y comunes en muchos idiomas del
mundo, y el vocabulario incluye palabras prestadas de varios idiomas alrededor
del mundo.

# Sobre este curso

Hay varias fuentes de aprendizaje de calidad para aprender toki pona. La más
importante (y la mejor, en mi opinión), es el [**libro oficial de toki
pona**](https://tokipona.org/) (también conocido como "pu") publicado en 2014
por Sonja Lang ella misma. No es gratis, pero es un libro bien escrito con
mucho texto adicional para leer, y explica el idioma muy bien.

Un video-curso divertido es la serie en YouTube ["12 days of sona pi toki
pona"](https://www.youtube.com/watch?v=4L-dvvng4Zc) por jan Misali. Cubre todo
el idioma en 12 videos cortos, cada uno mostrando 10 palabras del diccionario
oficial (psst: la estructura de mis páginas está inspirada por éste)

Previamente, el curso online "o kama sona e toki pona!" ("aprende toki pona!")
por Bryant Knight (también conocido como "jan Pije") sirve como otro recurso de
aprendizaje importante. Antes de ser retirada por su creador, era una de las
primeras páginas en toki pona disponibles online. Por lo tanto, tenía algunas
diferencias con respecto al uso de algunas palabras. Las versiones tempranas
del curso también han sido controvertidas por la inclusión de varias
declaraciones intolerantes o prejuiciosas en su contenido.

Mi objetivo es intentar presentar una versión que tenga en cuenta las maneras
diferentes en las que habla y escribe en toki pona la gente y la manera en que
es usado hoy en día. Algunas páginas incluirán secciones de "Diferencias
dialécticas", en las que estas diferencias serán documentadas. Algunas
diferencias más grandes serán descritas directamente. Proveeré mis opiniones
personales sobre algunas de estas diferencias, así que aunque este curso trata
de ser exhaustivo, no es imparcial.

La página numerada "cero" proveerá información básica sobre la ortografía y
pronunciación del idioma, y cada página después de esta introducirá 10 palabras
del diccionario de 120 palabras del idioma.

## Índice

### Páginas del curso

* [página 0 - ortografía y pronunciaciones](es_0.html)

* [página 1 - frases básicas](es_1.html)

* [página 2 - adjetivos](es_2.html)

* [página 3 - verbos y objetos](es_3.html)

* [página 4 - oh no! más vocabulario](es_4.html)

* [página 5 - esto y aquello](es_5.html)

* [página 6 - preposiciones y ubicaciones](es_6.html)

* [página 7 - interjecciones, preguntas, comandos y nombres](es_7.html)

* * [página 7a - más sobre cómo hacer palabras no oficiales](es_7a.html)

* [página 8 - lenguaje colorido](es_8.html)

* [página 9 - adjetivos complejos y contextos](es_9.html)

* [página 10 - pre-verbos y tiempo](es_10.html)

* [página 11 - números](es_11.html)

* [página 12 - el final de la cuenta atrás](es_12.html)

* [página 13 - edición especial de diccionario](es_13.html) **NUEVO**

### Páginas extra

* [página extra 1 - palabras viejas y nuevas](es_x1.html)
* * [descripción de algunas de estas palabras en toki pona](nimi_pi_pu_ala.html)

* [página extra 2 - otros sistemas de escritura](es_x2.html)
* * [sitelen pona](es_sitelen_pona.html)

* [diccionario](es_dictionary.html)

* [mi estilo personal](es_personal_style.html)

* [créditos y agradecimientos](credits.html) (Inglés)

### Contenido original

* [musi pi jan lawa moli (reglas del ajedrez)](chess.html)

* * [musi Soki (reglas del shogi) (en trabajo)](shogi.html)

* [lipu nasin pi musi Lisi Masan (Riichi Mahjong) (en trabajo)](riichi_mahjong.html)

* [ante toki tawa musi pi sitelen tawa (traducciones de videojuegos)](game_patches.html)

* * ["jan lawa pi linja ilo" ("LAN Master")](jan_lawa_pi_linja_ilo.html)

* * [ante toki pi jan Pije (traducciones de jan Pije)](game_patches_jp.html)

* ["nasin Juni pi ilo kon"](nasin_juni.html), una página corta sobre la
  "filosofía de Unix"

* ["o lawa e sona sina!"](o_lawa_e_sona_sina.html), un artículo corto sobre la
  centralización del internet

* [nasin tenpo](nasin_tenpo.html), un sistema de medición de tiempo que solo
  usa unos pocos números

### Contenido original (Inglés)

* [alfabeto fonético (EN/RU)](phonetic_alphabet.html)

* [disposición de teclado](keyboard.html)

* ["insa pi supa lape" -- una fuente original de sitelen pona](supalape.html)

* [lista de nombres de ciudades rusas/ucranianas/bielorrusas](city_names.html)

## Software (Inglés)

Here are links to some software that I personally found useful:

* ["ding" dictionary tool](http://www-user.tu-chemnitz.de/~fri/ding/) is a
simple-looking and easy to use dictionary tool that, by default, comes bundled
with German-to-English dictionaries, but toki pona-to-English files for it are
also available [on this
page](https://jan-lope.github.io/Toki_Pona_lessons_English/).

* [toki pona keyboard](https://github.com/timeopochin/tokiponakeyboard) for
  Android phones. Supports both Latin (with some sitelen pona font features) and
  sitelen emoji. (WIP)

## Otros cursos online (Inglés)

* [Jonathan Gabel's lessons](https://jonathangabel.com/toki-pona) teach both
  toki pona as a language and "sitelen sitelen", a rather ornamental-looking (if
  uncommon in general usage) writing system for toki pona.

## Recursos útiles (Inglés)

Apart from the above-mentioned book and courses, here are some good resources
and links for people who want to learn or use toki pona:

* [tokipona.net](http://tokipona.net) (warning: if your "HTTPS Everywhere" addon
  is set to "Encrypt All Sites Eligible", disable it for tokipona.net, or else
  it will redirect you to an unrelated website)

* ["lipu lili pona", a description of toki pona's rules and vocabulary on 2 
 pages](https://neocities.org/site/lipu-lili-pona)

* [Another description of toki pona's rules and vocab on 2
  pages](https://morr.cc/toki-pona-cheat-sheet/)

* [A toki pona dictionary with sitelen pona
 characters](https://theotherwebsite.com/tokipona/) by "The Other Website"

* [toki pona flashcards for Anki](https://ankiweb.net/shared/decks/toki%20pona)

* * [flashcards based on this
website](https://ankiweb.net/shared/info/204928497), by u/parentis\_shotgun

* [jan Sotan's description of his personal style, including the informal "tok'
  apona" dialect](https://github.com/ae-dschorsaanjo/lipu-lili-pi-toki-pona)

## Contenido original de otros (Inglés)

* ["musi lili"](https://musilili.net/), a page by jan Same

* [lipu kule, a blog with posts in toki pona by various
  people](https://lipukule.org/)

* ["lipu tenpo", a toki pona magazine](https://liputenpo.org/)

* [lipu Wikipesija, a recent project to make a toki pona
 Wikipedia](https://wikipesija.org) **NEW**

* [Russian TV: "Art Revolution" on conlangs](art_revolution.html)

### Grupos de discusión y chats (Inglés)

* [/r/tokipona subreddit](https://reddit.com/r/tokipona)

* ["ma pona pi toki pona" Discord server](https://discord.gg/Byqn5z9)

* [toki pona Telegram group](https://telegram.me/joinchat/BLVsYz92zHUp2h2TYp9kTA)
* * [IRC channel #tokipona on Freenode, bridged with the Telegram group](ircs://freenode.org:6697/#tokipona)

* ["kulupu pi toki pona" Telegram group](https://t.me/kulupupitokipona) (this
  one is usually more about talking _in_ toki pona)

---
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img
alt="Licencia Creative Commons" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br />Esta obra
está bajo una <a rel="license"
href="http://creativecommons.org/licenses/by-sa/3.0/">Licencia Creative Commons
Atribución-CompartirIgual 3.0 No portada</a>.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Licencia Creative Commons" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Esta obra
está bajo una <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia Creative Commons
Atribución-CompartirIgual 4.0 Internacional</a>.
