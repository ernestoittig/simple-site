% diccionario de toki pona
% /dev/urandom
% febrero 2022

Esta es una lista de todas las palabras oficiales en toki pona descritas en las
páginas 1 a 12, ordenadas alfabéticamente. Se puede hacer click sobre las
palabras para ir a la lección en la que fue introducida. Cada palabra está listada
con diferentes significados dependiendo de con qué partes de la oración se
puede usar.

Las definiciones de "adjetivos" aplican a tanto adjetivos como adverbios, ya
que la única diferencia entre los dos en toki pona es si se aplican a
sustantivos o verbos.

Un sustantivo siempre se puede usar como un adjetivo con el significado "de
[sustantivo]". Por ejemplo, "moku soweli" puede significar "comida animal" o
"carne", dependiendo del contexto.

> %note%
> Esto no es una copia del diccionario como está definido en el libro original
> o de otro curso. Algunas palabras pueden ser usadas como partes de la oración
> diferentes a las listadas aquí. También lista algunos usos no convencionales
> de las palabras, algunos de los cuales solo son útiles en combinación con
> otras palabras.
> 
> En toki pona, uno está invitado a inventar frases nuevas y significados
> alternativos a las palabras dependiendo del contexto. La regla más importante
> es que otros te entiendan.
>

---

#### [a](es_7.html)
* partícula: (interjección emocional, énfasis o confirmación)

#### [akesi](es_12.html)
* sustantivo: lagarto, reptil

> %warning%
> La palabra "akesi" antes también significaba "animal 'no adorable'", pero el
> segundo libro oficial de toki pona eliminó este significado.

#### [ala](es_2.html)
* sustantivo: nada
* adjetivo: no, ninguno, vacío
* numeral: cero

#### [alasa](es_12.html)
* verbo: cazar, recolectar
* verbo (poco convencional): buscar
* pre-verbo (poco convencional): intentar (hacer algo)

#### [ale/ali](es_2.html)
* sustantivo: todo, universo
* adjetivo: cada, todos, abundante
* numeral: todo/infinito (sistema simple), 100 (sistema complejo)

#### [anpa](es_7.html)
* sustantivo (viejo/no-pu): parte inferior
* adjetivo: inclinado, de rodillas, inferior, humilde, dependiente
* verbo sin obj: arrodillarse, inclinarse
* verbo con obj: conquistar, vencer

#### [ante](es_5.html)
* sustantivo: diferencia, cambio, (poco convencional) versión
* adjetivo: diferente, otro, cambiado
* verbo: cambiar

#### [anu](es_7.html)
* partícula: o

#### [awen](es_5.html)
* sustantivo (poco convencional): estabilidad, seguridad, espera
* adjetivo: mantenido, seguro, duradero, resiliente, esperando, manteniendo
* verbo: mantener, esperar, soportar, proteger
* pre-verbo: continuar (haciendo algo)

#### [e](es_3.html)
* partícula: (especifica un objeto)

#### [en](es_5.html)
* partícula: y (combina sujetos)

#### [esun](es_11.html)
* sustantivo: comercio, transacción, intercambio
* adjetivo: relacionado con el intercambio, comercial
* verbo: comerciar, intercambiar

#### [ijo](es_3.html)
* sustantivo: cosa, objeto, materia
* adjetivo: material, físico

#### [ike](es_1.html)
* sustantivo: mal
* adjetivo: malo, malvado, complejo, innecesario
* verbo (poco convencional): empeorar algo

#### [ilo](es_3.html)
* sustantivo: herramienta, máquina, dispositivo
* adjetivo: útil, (poco convencional) electrónico, metálico

#### [insa](es_7.html)
* sustantivo: entrañas, contenidos, centro, estómago
* adjetivo: central, dentro, entre

#### [jaki](es_8.html)
* sustantivo: mugre, basura
* adjetivo: sucio, desagradable, tóxico, antihigiénico
* verbo: ensuciar algo

#### [jan](es_2.html)
* sustantivo: persona, pueblo, humanidad, alguien
* adjetivo: humano, personal

#### [jelo](es_8.html)
* sustantivo/adjetivo: (el color) amarillo (y sus tonos)
* verbo: pintar algo de amarillo

#### [jo](es_4.html)
* sustantivo: (poco convencional) posesiones, propiedad
* verbo: tener/llevar/contener/sostener

#### [kala](es_4.html)
* sustantivo: pez, animal marino

#### [kalama](es_5.html)
* sustantivo: sonido, ruido
* adjetivo: sonoro, ruidoso, que hace ruido
* verbo: hacer ruido, recitar, tocar (un instrumento)

#### [kama](es_10.html)
* sustantivo: evento, llegada
* adjetivo: llegando, viniendo, futuro, convocado
* pre-verbo: convertirse en, comenzar el proceso de (hacer algo), lograr (algo), tener éxito en

#### [kasi](es_4.html)
* sustantivo: planta, pasto, hierba, hoja

#### [ken](es_10.html)
* sustantivo: habilidad, posibilidad, (poco convencional) derecho, libertad
* adjetivo (poco convencional): capaz, posible
* pre-verbo: poder (hacer algo)

#### [kepeken](es_6.html)
* sustantivo: uso, (poco convencional) práctica
* verbo con obj: usar algo
* preposición: (hacer algo) usando, con la ayuda de

#### [kili](es_1.html)
* sustantivo: fruta, vegetal, seta

#### [kiwen](es_4.html)
* sustantivo: objeto duro, metal, piedra, sólido
* adjetivo: duro, metálico, sólido

#### [ko](es_4.html)
* sustantivo: polvo, arcilla, pasta, semi sólido

#### [kon](es_12.html)
* sustantivo: aire, esencia, espíritu, (poco convencional) gas
* adjetivo: invisible, efímero, (poco convencional) gaseoso

#### [kule](es_8.html)
* sustantivo: color, (raro) género
* adjetivo: colorido, pintado
* verbo: pintar algo de un color

#### [kulupu](es_5.html)
* sustantivo: grupo, comunidad, compañía, sociedad, nación, tribu
* adjetivo: comunal, social

#### [kute](es_7.html)
* sustantivo: oído, oreja, audición
* adjetivo: que suena a
* verbo: oír, escuchar, obedecer

#### [la](es_9.html)
* partícula: "si/cuando" (introduce contexto)

#### [lape](es_5.html)
* sustantivo: sueño, descanso
* adjetivo: durmiente, descansando
* verbo: dormir, descansar

#### [laso](es_8.html)
* sustantivo/adjetivo: (el color) azul, verde (y sus tonos)
* verbo: pintar algo de azul/verde

#### [lawa](es_7.html)
* sustantivo: cabeza, mente
* adjetivo: principal, primario, controlador, gobernante
* verbo: encabezar, controlar, dirigir, guiar, ser dueño, gobernar

#### [len](es_9.html)
* sustantivo: tela, ropa, tejido, capa de privacidad
* adjetivo: vestido, hecho de tela
* verbo: vestir, proveer una capa de privacidad

#### [lete](es_9.html)
* sustantivo: frío
* adjetivo: frío, fresco, crudo
* verbo: enfriar

#### [li](es_1.html)
* partícula: (entre sujeto y verbo/adjetivo)

#### [lili](es_1.html)
* sustantivo: pequeñez
* adjetivo: pequeño, poco, joven
* verbo: encoger

#### [linja](es_9.html)
* sustantivo: objeto largo flexible, hilo, cuerda, cabello

#### [lipu](es_3.html)
* sustantivo: objeto plano, libro, documento, papel, página, registro, sitio web
* adjetivo: plano, usado como lipu, de lipu

#### [loje](es_8.html)
* sustantivo/adjetivo: (el color) rojo (y sus tonos)
* verbo: pintar algo de rojo

#### [lon](es_6.html)
* sustantivo: verdad, vida, existencia
* adjetivo: real, verdadero, presente, existente
* verbo sin objeto: es verdad, existe
* preposición: en, sobre

#### [luka](es_9.html)
* sustantivo: mano, brazo
* numeral: 5 (sistema complejo)

#### [lukin](es_3.html)
* sustantivo: ojo, visión
* adjetivo: que se ve, visual
* verbo: mirar, ver, leer
* pre-verbo: buscar (hacer algo)

#### [lupa](es_10.html)
* sustantivo: agujero, puerta, orificio, ventana

#### [ma](es_4.html)
* sustantivo: tierra, intemperie, territorio, país

#### [mama](es_2.html)
* sustantivo: padre, madre, ancestro, creador, origen, cuidador
* verbo: crear, ser padre, cuidar

#### [mani](es_11.html)
* sustantivo: dinero, animal domesticado grande
* adjetivo: (poco convencional) rico

#### [meli](es_2.html)
* sustantivo: mujer, esposa
* adjetivo: femenino

#### [mi](es_1.html)
* sustantivo: yo, me, nosotros, nos 
* adjetivo: mi, nuestro

#### [mije](es_2.html)
* sustantivo: hombre, marido
* adjetivo: masculino

#### [moku](es_2.html)
* sustantivo: comida
* adjetivo: comestible, de la comida
* verbo: comer, beber, tragar

#### [moli](es_8.html)
* sustantivo: muerte
* adjetivo: muerto, muriendo
* verbo: matar

#### [monsi](es_6.html)
* sustantivo: parte de atrás, posterior, nalgas
* adjetivo: atrás, posterior

#### [mu](es_7.html)
* (cualquier sonido de animal)

#### [mun](es_11.html)
* sustantivo: luna, estrella, objeto en el cielo de la noche
* adjetivo: lunar, estelar

#### [musi](es_9.html)

* sustantivo: juego, arte
* adjetivo: entretenido, artístico, divertido
* verbo: divertir, jugar, divertirse

#### [mute](es_5.html)
* sustantivo: cantidad
* adjetivo: mucho, más
* numeral: 3 o más (sistema simple), 20 (sistema complejo)

#### [nanpa](es_11.html)
* sustantivo: número
* adjetivo: -ésimo (indicador ordinal), matemático, numérico, 
    (poco convencional) digital

#### [nasa](es_8.html)
* adjetivo: raro, inusual, extraño, ebrio

#### [nasin](es_10.html)
* sustantivo: camino, ruta, calle, dirección, manera, tradición
* adjetivo: de calle, que sigue la manera/dirección/tradición
* verbo (poco convencional): guiar, mostrar el camino

#### [nena](es_10.html)
* sustantivo: colina, montaña, botón, protuberancia, nariz
* adjetivo: serrano, montañoso, abultado

#### [ni](es_5.html)
* sustantivo/adjetivo: esto, eso, aquello

#### [nimi](es_7.html)
* sustantivo: palabra, nombre

#### [noka](es_6.html)
* sustantivo: pie, pierna, fondo, parte de abajo, debajo de (algo)

#### [o](es_7.html)
* partícula: (para dirigirse a gente, dar órdenes)

#### [olin](es_3.html)
* sustantivo: amor, compasión, afecto, respeto
* adjetivo: amado, favorito, respetado
* verbo: amar, respetar

#### [ona](es_1.html)
* sustantivo: él, ella, esto 
* adjetivo: su (de él/ella)

#### [open](es_10.html)
* sustantivo: comienzo
* adjetivo: inicial, comenzando
* verbo: empezar, comenzar, encender
* pre-verbo: empezar (a hacer algo)

#### [pakala](es_5.html)
* sustantivo: daño, error
* adjetivo: roto, incorrecto
* verbo: romper, cometer un error
* partícula: (maldición genérica)

#### [pali](es_3.html)
* sustantivo: trabajo, labor
* adjetivo: trabajando
* verbo: trabajar (en), hacer

#### [palisa](es_9.html)
* sustantivo: objeto sólido largo, rama, palo, (poco convencional) longitud
* adjetivo: largo

#### [pan](es_12.html)
* sustantivo: pan, grano, cereal, arroz, pizza

#### [pana](es_3.html)
* adjetivo: (poco convencional) dado, enviado, soltado
* verbo: dar, enviar, emitir, soltar

#### [pi](es_9.html)
* partícula: "de" (reagrupa dos o más modificadores)

#### [pilin](es_12.html)
* sustantivo: corazón, sentimiento, toque, sentido
* adjetivo: sintiente, basado en el tacto
* verbo: tocar, pensar, sentir

#### [pimeja](es_8.html)
* sustantivo: (el color) negro (y sus tonos), sombra
* adjetivo: negro, oscuro
* verbo: pintar algo de negro, hacer sombra

#### [pini](es_10.html)
* sustantivo: final
* adjetivo: final, completado, terminado, pasado (con tenpo)
* verbo: terminar, completar, cerrar
* pre-verbo: dejar/parar de hacer algo

#### [pipi](es_4.html)
* sustantivo: insecto, bicho

#### [poka](es_6.html)
* sustantivo: cadera, costado, alrededores
* adjetivo: alrededor, cerca, al costado de uno

#### [poki](es_11.html)
* sustantivo: caja, contenedor, cuenco, taza, cajón
* verbo (poco convencional): poner en una caja

#### [pona](es_1.html)
* sustantivo: bondad, simplicidad
* adjetivo: bueno, simple, amigable, pacífico
* verbo: mejorar, arreglar

#### [pu](es_12.html)
* sustantivo: el libro oficial de toki pona
* adjetivo: como en el libro oficial de toki pona
* verbo: interactuar con el libro oficial de toki pona

> %note%
> El libro oficial de toki pona solo define el significado de verbo de "pu".
> (Aunque la frase "pu la" sí es usada para decir "en este libro".) Algunas
> personas en la comunidad de toki pona prefieren usar solo el verbo, mientras
> que otros usan los otros también.

#### [sama](es_6.html)
* sustantivo: similaridad, hermano/hermana (de alguien)
* adjetivo: similar, como, hermano
* preposición: como, igual que

#### [seli](es_5.html)
* sustantivo: calor, reacción química, fuente de calor
* adjetivo: cálido, caliente
* verbo: calentar

#### [selo](es_9.html)
* sustantivo: forma exterior, capa exterior, carcasa, piel, borde
* adjetivo: exterior

#### [seme](es_7.html)
* partícula: ¿qué? ¿cuál? (para preguntas)

#### [sewi](es_6.html)
* sustantivo: área arriba, cima, parte más alta, cielo, dios
* adjetivo: alto, arriba, divino, sagrado

#### [sijelo](es_9.html)
* sustantivo: cuerpo, estado físico, torso
* adjetivo: físico, de sijelo

#### [sike](es_11.html)
* sustantivo: círculo, bola, ciclo, rueda, (con tenpo) año
* adjetivo: redondo, circular, esférico, de un año
* verbo: hacer un círculo alrededor de, rodear

#### [sin](es_11.html)
* sustantivo: novedad, adición, (poco convencional) actualización, condimento
* adjetivo: nuevo, adicional, fresco, extra
* verbo: añadir, actualizar

#### [sina](es_1.html)
* sustantivo: tú
* adjetivo: tuyo 

#### [sinpin](es_6.html)
* sustantivo: cara, parte del frente, frente, pared
* adjetivo: de la cara, del frente

#### [sitelen](es_4.html)
* sustantivo: símbolo, imagen, escritura
* adjetivo: simbólico, escrito, grabado
* verbo: escribir, dibujar, grabar

#### [sona](es_10.html)
* sustantivo: conocimiento, información
* adjetivo: conocido
* verbo: saber
* pre-verbo: saber (cómo hacer algo)

#### [soweli](es_1.html)
* sustantivo: mamífero de tierra, animal

#### [suli](es_1.html)
* sustantivo: tamaño, grandeza
* adjetivo: grande, pesado, alto, importante, adulto
* verbo: crecer

#### [suno](es_11.html)
* sustantivo: sol, luz, brillo, fuente de luz
* adjetivo: solar, brillante
* verbo: iluminar, brillar

#### [supa](es_12.html)
* sustantivo: superficie horizontal

#### [suwi](es_2.html)
* sustantivo: (poco convencional) dulces, fragancias
* adjetivo: dulce, perfumado, adorable

#### [tan](es_6.html)
* sustantivo: causa, razón, origen
* adjetivo: original
* verbo con objeto (poco convencional): causar
* preposición: desde, porque

#### [taso](es_12.html)
* partícula (al principio de la oración): pero, sin embargo
* adjetivo: único

#### [tawa](es_6.html)
* sustantivo: movimento
* adjetivo: ambulante, movedizo
* verbo: mover(se)
* preposición: a, para, desde la perspectiva de

#### [telo](es_3.html)
* sustantivo/adjetivo: agua, fluido, líquido
* adjetivo: mojado, fluido, líquido
* verbo: regar, limpiar

#### [tenpo](es_10.html)
* sustantivo: tiempo, momento, ocasión
* adjetivo: temporal

#### [toki](es_4.html)
* sustantivo: habla, conversación, lenguaje
* adjetivo: verbal, conversacional
* verbo: hablar, decir, usar el idioma, pensar

#### [tomo](es_3.html)
* sustantivo: casa, edificio, estructura, espacio interior, habitación
* adjetivo: adentro

#### [tonsi](x1.html)
* sustantivo: persona de género no binario, persona trans
* adjetivo: que no conforma del género, trans

> %warning%
> "tonsi" es la única palabra no oficial en esta lista. Fue creada por la
> comunidad luego de que se publique el libro oficial y es la palabra no
> oficial más aceptada.

#### [tu](es_11.html)
* numeral: 2
* sustantivo: división
* adjetivo: dividido
* verbo: dividir

> %warning%
> Usar "tu" al final de una frase nominal está generalmente asociado con el
> número 2\. El significado "dividido" se especifica normalmente usando una
> partícula "li":
>
> kulupu tu -- dos comunidades
>
> kulupu li tu. -- la comunidad está dividida

#### [unpa](es_8.html)
* sustantivo: sexo
* adjetivo: sexual
* verbo: tener sexo con

#### [uta](es_12.html)
* sustantivo: boca, labios
* adjetivo: oral

#### [utala](es_2.html)
* sustantivo: pelea, batalla, reto, guerra
* adjetivo: agresivo, bélico
* verbo: pelear, batallar, retar

#### [walo](es_8.html)
* sustantivo: el color blanco (y sus tonos)
* adjetivo: blanco, brillante/claro
* verbo: pintar algo de blanco

#### [wan](es_11.html)
* numeral: 1
* sustantivo: parte (de algo)
* adjetivo: unido, casado
* verbo: unir, casar(se)

> %warning%
> El uso de "wan" al final de una frase nominal está generalmente asociado con
> el número 1\. El sentido "unido" usualmente se especifica usando una partícula
> "li":
>
> kulupu wan -- una comunidad
>
> kulupu li wan. -- la comunidad está unida
>
> kulupu mute wan -- 21 comunidades (sistema de numeración complejo)
> 
> kulupu mute li wan -- varias (o 20) comunidades están unidas.

#### [waso](es_4.html)
* sustantivo: pájaro, animal volador

#### [wawa](es_2.html)
* sustantivo: fuerza, poder, energía
* adjetivo: fuerte, poderoso, energético

#### [weka](es_12.html)
* sustantivo: ausencia, lejanía
* adjetivo: ausente, fuera, remoto
* verbo: remover, deshacerse de

#### [wile](es_10.html)
* sustantivo: deseo, necesidad
* adjetivo: deseado, necesitado, requerido
* verbo: querer/necesitar algo
* pre-verbo: querer/necesitar hacer algo

---

[Página principal](index.html)
