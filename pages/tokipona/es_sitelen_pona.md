% sitelen pona descripción y ejemplos
% /dev/urandom
% febrero 2022

## sitelen pona

"sitelen pona" ("escritura simple" o "escritura buena") es un sistema de
escritura logográfico diseñado para toki pona por su creadora, Sonja Lang. 

> %info%
> La parte del libro oficial que describe sitelen pona fue publicada con una
> licencia no comercial [CC-BY-NC
> 4.0](https://creativecommons.org/licenses/by-nc/4.0/), Por lo tanto está
> disponible online en otros cursos, como la [página de jan
> Pije](http://tokipona.net/tp/janpije/hieroglyphs.php) sobre el sistema, que
> describe casi exactamente lo mismo que el libro oficial.
>

### Sistemas logográficos

En un sistema logográfico, **cada carácter generalmente representa una
palabra** (a veces una frase). El sistema logográfico más conocido es el de los
sinogramas (o caracteres Han), usados en el chino y (además de sus propios
sistemas) el japonés y coreano.

> %info%
> Los sistemas logográficos funcionan bien con lenguajes en los que las
> palabras tienen casi nada de flexión (cambian muy poco, o nada, basadas en la
> gramática), y la gramática se basa en cambio en juntar palabras existentes
> (un idioma de este tipo se llama "aislante"). Los idiomas chinos coinciden
> con esa idea muy bien, al igual que toki pona.
>

Pero dado que el diccionario básico de toki pona solo usa **120 palabras** (más
unas cuantas adiciones comunitarias), un sistema logográfico para toki pona
también se hace **significativamente más fácil de aprender** y usar que el del
chino, que requiere saber por lo menos 1500 caracteres para llegar a la
fluidez. Además, la mayoría de caracteres en sitelen pona representan
visualmente las palabras que significan -- ej. "lawa" que significa "cabeza",
es literalmente un símbolo de una cabeza con un gorro puesto. "nanpa" que
significa "número", está basado en el símbolo numeral "\#", etc. (Hasta cierto
punto, esto es cierto para un número de sinogramas también.)

### Tabla de sitelen pona

![Tabla de caracteres de sitelen pona](/tokipona/sitelen_pona.gif)

> Esta tabla muestra todos los caracteres de sitelen pona usados para las 120
> palabras oficiales, las palabras importantes del segundo libro ("nimi ku
> suli"), como también algunos caracteres comúnmente usados para palabras
> adicionales ("nimi sin") y representaciones alternativas ("nasin lukin
> ante"). Algunos de los caracteres alternativos son mis diseños originales --
> éstos están marcados con un pequeño triángulo rojo en la esquina inferior
> derecha.

Como el alfabeto latino, se escribe de izquierda a derecha y de arriba a abajo.
Cada palabra en toki pona se escribe usando su carácter, sin espacios extra
entre palabras.

Un carácter de adjetivo se puede poner adentro o arriba/abajo de un carácter de
sustantivo para representar una frase nominal. Este tipo de caracteres se
conocen comúnmente como "caracteres compuestos".

> %info%
> Se puede ver que el "logo" de toki pona, usado en la portada del libro
> oficial en la mayoría de sitios web para representarlo (incluyendo este), es,
> en realidad, el carácter compuesto de sitelen pona para "toki pona", con el
> símbolo de "pona" escrito dentro del símbolo de "toki".

Las palabras no oficiales se escriben dentro de un cartucho (una forma redondeada que rodea todos los caracteres), con caracteres de palabras que empiezan con sus primeras letras. Por ejemplo, en la página del enlace al principio (y usada en el libro oficial), "ma Kanata" se escribe "ma [kasi alasa nasin awen telo a]". (En algunas fuentes, el cartucho puede ser reemplazado con paréntesis o corchetes entre los caracteres.)

### sitelen pona como es comúnmente usado

> %info%
> La información en esta parte no es parte del diseño original de sitelen pona.
> Está basada completamente en cómo sitelen pona es usado por la comunidad de
> toki pona.

Las oraciones son separadas por un punto o un espacio. Toda la otra puntuación
(comas, dos puntos, etc.) es omitida o escrita con sus caracteres
correspondientes (ya que en prácticamente todos los casos, su presencia o
ausencia no cambia el significado de una oración).

Las palabras añadidas por la comunidad de toki pona normalmente tienen su
propios caracteres y no son escritas como palabras no oficiales.

Ya que el signo de pregunta se usa para el carácter de "seme", las oraciones de
pregunta pueden terminar con un punto (o un signo de pregunta más pequeño).

Algunas personas usan caracteres compuestos dentro de cartuchos para escribir
sílabas enteras. Por ejemplo, [este
tweet](https://twitter.com/qvarie/status/1291755067851251712) muestra la
palabra no oficial "Nijon" ("Japón") escrita como tres caracteres: "nena-ilo",
"jan-oko" y "nena".

### Ejemplos

Aquí hay un texto básico escrito en sitelen pona.

![wan ni pi lipu ni li sitelen kepeken sitelen pona. sina ken ala ken sona e ni.
/ toki pona li jo e nimi pi mute lili. tan ni nasin sitelen ni li ken: sitelen
wan li toki e nimi wan. sitelen ale li lukin sama kon
ona.](/tokipona/sitelen_pona_example.png)

[Traducción](es_answers.html#sp)

Para algunos otros textos escritos en sitelen pona, incluyendo una página que
trata de enseñar a leerlo sin usar otro sistema de escritura, mirar el sitio
web ["tomo pi sitelen pona"](https://davidar.github.io/tp/) por jan Tepu.

### Fuentes

El texto anterior está mostrado usando una fuente llamada "linja pimeja". Sin
embargo, para mostrar texto en sitelen pona online, hay muchas otras opciones.
Aquí están algunas de las más comunes.

 * Una fuente llamada "[linja pona](musilili.net/linja-pona/)" es
   característica por su diseño básico y compatibilidad con muchísimos
   caracteres compuestos diferentes. Es la opción más popular.

 * "[sitelen pona pona](https://jackhumbert.github.io/sitelen-pona-pona/)" es
   una fuente que muestra algunos caracteres bastante diferentes del sitelen
   pona regular, pero se ve bien en tamaños de fuente distintos y no requiere
   ninguna modificación a texto en toki pona para verse bien. En particular,
   esta es mi fuente favorita.

> %info%
> Algunas páginas en este sitio web pueden ofrecer la habilidad de cambiar entre
> texto en sitelen pona y el alfabeto latino. La segunda opción usará la fuente
> "sitelen pona pona", ya que funciona con texto en toki pona sin
> modificaciones y muestra bien textos que usan palabras que no son pu, no
> oficiales o que directamente no están en toki pona.
>

 * "[linja suwi](https://linjasuwi.ap5.dev/)" es una fuente recientemente
   diseñada por alienpirate5. Contiene caracteres bien dibujados y varias
   palabras creadas por la comunidad.

 * Yo también he diseñado una fuente para sitelen pona, llamada "[insa pi supa
   lape](supalape.html)". Está basada en la fuente "Bedstead" (de ahí el
   nombre) y usa el mismo algoritmo para convertir mapas de bits pequeños de
   diferentes caracteres a una fuente de vectores funcional.

#### Ejemplos de fuentes diferentes

>
> sitelen ni li sitelen kepeken sitelen pona.
>
> sina ken ala ken sona e ona.
>
> ma Kanata li suli.
>
> jan pi sona mute li pali pona.
>

* linja pona:

![](/tokipona/lpona.png)

* linja pona (sílabas usando caracteres compuestos):

![](/tokipona/lpona2.png)

* linja pimeja:

![](/tokipona/lpimeja.png)

* sitelen pona pona:

![](/tokipona/spp.png)

* insa pi supa lape:

![](/tokipona/insa.png)

### sitelen emoji / sitelen pilin

Un sistema llamado "sitelen emoji" (o "sitelen pilin") adapta sitelen pona
usando un emoji por cada carácter de sitelen pona. Esto hace posible usarlo en
la mayoría de navegadores y apps de mensajería.

* [Página oficial](https://sites.google.com/view/sitelenemoji)

* [Descripción del sistema, tabla de caracteres de emoji de Windows y texto de ejemplo](https://omniglot.com/conscripts/sitelenemoji.htm)

---

[Página sobre otros sistemas de escritura alternativos](es_x2.html)

[Página principal](index.html)


