% estilo personal en toki pona
% /dev/urandom
% febrero 2022

La gente tiene ideas diferentes de cómo se supone que tiene que funcionar toki pona. Dada la gramática y vocabulario intencionalmente minimalista del lenguaje, es de esperar. Aquí está una lista de mis preferencias personales y frases que uso comúnmente.

* En oraciones que solo tienen "mi" o "sina" como sujeto, pero varios
  predicados (verbos o adjetivos que en otro caso estarían separados por "li"),
  yo separo la oración en dos:

  > mi pali. mi moku. - Trabajo y como.

* Trato de evitar usar "en" en cualquier lado que no sea el sujeto, pero tolero
  usarlo en frases que sigan "pi".

* Cuando uso "kepeken" como verbo ("usar") en vez de una preposición ("usando,
  con ayuda de"), incluyo el marcador de objeto "e" de la misma manera que lo
  haría con otros verbos.

* Puedo insertar comas como pausas para diferenciar entre frases ambiguas o
  para ayudar en la lectura de oraciones posiblemente confusas. Por ejemplo:

  > mi pana e tomo tawa sina. - Doy tu auto.
  
  > mi pana e tomo, tawa sina. - Te doy una casa.

  > mi pana e tomo tawa, tawa sina. - Te doy un auto.

* Inserto comas después de la en todas las circunstancias:

  > ken la, mi ken pali. - Tal vez puedo trabajar.

  > tomo pali li open la, mi ken pali. - Si la oficina está abierta, puedo trabajar.

* Uso "open" y "pini" como pre-verbos que significan "empezar (a hacer algo"
  "terminar/parar (de hacer algo").

* Cuando un numeral se usa como número, normalmente lo escribo con números
  arábigos. Si es un número ordinal, la palabra "nanpa" puede ser representada
  con un signo de numeral (`#`).

* No uso "pi" antes de "nanpa" su le sigue un número ordinal.

* Uso "pu" como todas las partes de la oración, no solo como un verbo.

[Página principal](index.html)
