% toki pona página extra 1 - palabras viejas y nuevas
% /dev/urandom
% febrero 2022

Todas las palabras que han sido descritas en las páginas de 1 a 12 están
presentes en el libro de toki pona oficial. Las palabras en la [página
13](es_13.html) son descritas como importantes en el segundo libro de toki pona
oficial. Sin embargo, hay algunas palabras adicionales que fueron usadas antes
y no aparecieron en el libro, o palabras que fueron creadas por la comunidad
después. A diferencia de "palabras no oficiales" usadas para nombres propios
(véase [página 7](es_7.html)), éstas son tratadas como palabras de toki pona
nativas y no se escriben con mayúscula.

El documento ["nimi ale pona"][nap] es una lista más o menos exhaustiva de todas las palabras en toki pona que son, o han sido, usadas

[nap]:https://docs.google.com/spreadsheets/d/1t-pjAgZDyKPXcCRnEdATFQOxGbQFMjZm-8EvXiQd2Po

Ésta página, en cambio, describirá todas las palabras que, desde mi punto de
vista, parecen mínimamente comunes en el uso en línea, como también la manera
en la que algunas de las 120 palabras oficiales son usadas alternativamente
dentro de la comunidad.

Por supuesto, dado que parte de la idea de toki pona es reducir el número de palabras
y quitar ideas innecesarias, cualquier uso de este tipo será algo
controvertido. Trataré de proveer mis propias opiniones sobre las palabras en
esta lista.

## Sinónimos: kin, namako y oko

Antes de que se publique el libro de toki pona oficial, había ciertas palabras
que eran usadas comúnmente, pero que tenían significados que eran demasiado
similares o innecesarios. Pero en vez de ser removidas, estas palabras fueron
añadidas como sinónimos de otras palabras.

La palabra "kin" es descrita como un sinónimo de "a", pero mientras que "a" es
una expresión más genérica de emoción, "kin" se usaba como una palabra de
énfasis similar a "de hecho",  "en efecto" o "también" -- enfatizando el
sentido literal de la oración, no el sentido emocional. Yo creo que este
sentido está cubierto bastante bien por las palabras "a" (como indicador
emocional) y "mute" (como un tipo de énfasis).
 
La palabra "namako" se usaba para significar "adición" o "condimento". En el
libro oficial se listó como un sinónimo de "sin", la palabra que significa
"nuevo", "extra" o "adicional". Mientras que las dos palabras sí tienen
significados separados, creo personalmente que "sin", especialmente cuando se
usa como un sustantivo o en una frase nominal como en "sin moku" (adición de
comida), puede usarse para expresar la misma idea muy bien.

La palabra "oko" está listada como un sinónimo de "lukin". Cuando estas eran
dos palabras diferentes, "oko" significaba específicamente "ojo", mientras que
"lukin" significaba "vista" o "visión". Dado que "kute" significa tanto
"audición" como "oído", esta me parece la decisión correcta.

(*lipu ku* re-introduce estas palabras como palabras de toki pona propiamente dichas.)

## Palabras eliminadas

También hay algunas palabras que son usadas ocasionalmente en la comunidad,
aunque fueron completamente eliminadas del diccionario en el momento de la
publicación del libro oficial.

Empecemos con las más comunes:

La palabra "apeja" se describe como "vergüenza" o "culpa". Describir ese concepto usando solo las palabras del libro oficial es un poco difícil, así que algunas personas continúan usándola.

La palabra "kipisi" tiene los significados de "dividir, cortar, parte". Estos
significados han sido fusionados con las palabras "tu" (dividir) y "wan"
(parte, elemento), pero todavía se usa comúnmente por la comunidad, e ideas
para un carácter de *sitelen pona* han sido propuestas (la más común parece
`%`).

La palabra "leko" (probablemente derivada del nombre de la marca de juguetes Lego) significa "bloque", "cuadrado" o a veces "escaleras". No hay ninguna palabra o frase que pueda sustituirla, así que todavía tiene uso ocasional cuando es necesaria.

La palabra "monsuta" significa "monstruo" o "miedo". Como con "apeja", es algo que algunos encuentran difícil de describir (especialmente porque puede ser descrito de muchas maneras), así que se usa una palabra vieja.

Aquí hay algunas otras palabras que han sido prácticamente abandonadas hoy en
día, pero que pueden ser usadas en textos antiguos.

La palabra "kapa" era una palabra temprana que significaba "montaña, colina"
que terminó siendo reemplazada por "nena".

La palabra "kapesi" significaba otro término de color, describiendo los colores gris, marrón y, a veces, café, pero fue eliminada, ya que frases como "pimeja walo" y "pimeja jelo" podían usarse para describir gris y marrón fácilmente.

La palabra "majuna", que significa "viejo", era otra palabra temprana que
terminó siendo eliminada. Ya que puede ser descrita usando palabras que se
refieren al tiempo relativamente fácil, ya no se usa comúnmente. Por ejemplo:

> ona mute li majuna. -- Ella es vieja.

> tenpo mute la ona mute li lon. -- Él ha existido por mucho tiempo.

La palabra "misikeke" significa "medicina" o "cura".

La palabra "pasila" era una palabra diferente para "fácil", pero fue eliminada
del vocabulario de toki pona antes de que la primera versión web fuera
publicada en 2001.

La palabra "pake" era un verbo que significaba "parar, dejar de" derivada de la
palabra del francés acadiano "barrer" (que significa "cerrar con llave"), pero
fue eliminada, probablemente porque su significado se puede expresar con "pini"
(parar, terminar) o con "awen" (mantener, quedarse).

La palabra "pata" significaba "hermano, hermana", pero ahora se expresa
comúnmente como "jan sama".

La palabra "powe", que significa "falso", se eliminó, ya que es fácil derivar
su significado con frases basadas en "lon ala" (no existe) o "sona
ike" (conocimiento malo, desinformación).

También estaban las palabras "tuli" y "po", que eran los numerales 3 y 4. Han
sido reemplazadas con las frases "tu wan" y "tu tu".

(*lipu ku* re-introduce "kipisi", "leko", "monsuta" y "misikeke".)

## Dirección

Mientras que toki pona tiene palabras para "arriba", "abajo", "adelante" y "atrás", no tiene
palabras para "izquierda" o "derecha", en cambio solo tiene una palabra para "lado".

Algunas personas han inventados frases basadas en que la mayoría de personas
escriben con su mano derecha ("poka pi luka sitelen" = derecha, "poka pi luka
sitelen ala" = izquierda), que tienen su corazón en el lado izquierdo del
cuerpo ("poka pilin" = izquierda, "poka pilin ala" = derecha), escriben texto
de izquierda a derecha ("poka open" = izquierda, "poka pini" = derecha).

Todos estos, obviamente, no son 100% correctos en todas las situaciones: hay gente que son zurdos, que tienen su corazón en el lado derecho de su cuerpo (dextrocardia) o que escriben de derecha a izquierda. (Aunque, para ser justos, todos los sistemas principales *para escribir toki pona* -- el alfabeto latino, sitelen pona y sitelen sitelen -- se escriben de izquierda a derecha.)

El documento "nimi ale pona" lista dos palabras *post-pu* que se supone que son
más específicas: "soto" para izquierda y "teje" (previamente "te") para
derecha. Yo personalmente pienso que estas palabras pueden ser necesarias en
caso de que se necesite hacer una distinción entre izquierda y derecha, pero en
la mayoría de casos, es mejor evitar usarlas.

## Género y sexualidad

Están las palabras "mije" y "meli" que significan "masculino" y "femenino" respectivamente. Sin embargo, hay algunas personas que no son ni exclusivamente hombres ni exclusivamente mujeres, o que nacieron biológicamente ninguno de los dos.

La palabra "tonsi" fue creada para describir a esas personas, o, en algunos contextos, personas trans o cualquiera en la comunidad LGBT.

Adicionalmente, la palabra "kule" (color) a veces tiene un significado adicional --
a veces "género", a veces "LGBT". Al principio, puede traer preguntas, como "¿qué pasa con frases como 'personas de color'?", pero en la comunidad de toki pona, la raza o color de piel de una persona se suele mencionar directamente (ej. "persona blanca" es "jan pi selo walo", "persona de piel blanca").

También se pueden ver las palabras "mijomi" y "melome". Éstas fueron creadas
como abreviaciones de "mije olin mije" (hombres que aman a hombres) y "meli
olin meli" (mujeres que aman a mujeres) y tienen el mismo propósito que las
abreviaciones en inglés "mlm" y "wlw".

## Palabras misceláneas

Otras palabras fueron inventadas para describir conceptos que pueden ser muy
largos de explicar. La palabra "linluwi", por ejemplo, significa "internet" o
"la web".

La palabra "kili" colectivamente significa frutas, vegetales y setas, pero para hablar de setas específicamente, la palabra "soko" se usa ocasionalmente.

Hay una palabra para "dar", pero no hay una palabra para "obtener" (que se
puede aproximar con "kama jo"). La palabra "lanpan" provee ese significado
(junto con "tomar" y "conquistar"). La popularidad de la palabra puede estar
relacionada con el documento ["lanpan pan"][lpdoc] [(link de discord al
pdf)][lppdf], que es una traducción corta/resumen de *"La conquista del pan"*
por Piotr Kropotkin.

[lpdoc]: https://docs.google.com/document/d/1Pz7rvn7LXPJmJZ6AJxEWa6WuOJys8KHioXLtYkj0k-Q/edit
[lppdf]: https://discord.com/channels/301377942062366741/301380012156911616/584171157448687628

## Palabras de broma

Además, hay algunas palabras que fueron creadas como bromas por Sonja Lang ella
misma. En el documento "nimi ale pona", están listadas como "w.o.g. Sonja". El
más común es "kijetesantakalu", que se refiere a mapaches y otros animales de
la familia Procyonidae.

Otras palabras incluyen "mulapisu" para pizza y "yupekosi" para "revisar tu
trabajo viejo solo para empeorarlo"; nótese que toki pona no usa la letra "y"
no se sabe cómo pronunciar esta palabra.

## Tabla de vocabulario de las palabras adicionales más comunes

Esta lista está basada en la [tabla de frecuencia de palabras no-pu][freq] por
usuario de reddit `qwertyter`, con algunos cambios basados en experiencia
personal.

[freq]: https://docs.google.com/spreadsheets/d/1dGd4do1Jk2L2NwW5l7tLgSajAVkUqO0z2UHGu4_Sq_M

La columna de alternativas lista qué palabras y frases se pueden usar para
expresar ideas similares usando solo vocabulario oficial. No todas estas pueden
ser usadas como la misma parte de la oración (ej. usar "tu" usar tu como un
adjetivo sin un "li" para decir "dividido" puede ser fácilmente confundido con
el número "dos".)

| palabra | significado                        | alternativas          |
|---------|------------------------------------|-----------------------|
| kin     | énfasis ("de hecho", "también")    | a, mute               |
| monsuta | miedo, monstruo                    |                       |
| lanpan  | tomar, obtener, recibir            | kama jo (kama jo utala) |
| oko     | ojo                                | lukin                 |
| tonsi   | no binario / transgénero, LGBT     |                       |
| kipisi  | dividir, cortar                    | tu                    |
| namako  | adicional, condimento              | sin                   |
| kijetesantakalu | mapache                    |                       |
| leko    | cuadrado, ladrillo, escaleras      |                       |
| powe    | falso                              | sona ike, ... li lon ala |
| apeja   | vergüenza, culpa                   |                       |
| majuna  | viejo                              | pi tenpo mute pini    |
| pake    | parar, bloquear                    | pini, pali ala        |
| linluwi | internet                           |                       |
| soko    | seta                               | kili                  |

## "nimi pi pu ala"

En la página llamada "[nimi pi pu ala](nimi_pi_pu_ala.html)" puedes ver mi
intento de describir algunas de estas palabras usando solo el vocabulario
oficial de toki pona. A veces esto es fácil, a veces difícil. Leyendo esta
lista puedes decidir por ti mismo si vas a usar estas palabras o no.

[Página principal](index.html)
