% toki pona página extra 2 - otros sistemas de escritura
% /dev/urandom
% febrero 2022

Mientras que el sistema de escritura más común para toki pona es sin duda el
alfabeto latino, otros han sido adaptados a, o creados específicamente para,
toki pona.

## Sistemas de escritura adaptados

toki pona usa solo 5 vocales y 9 consonantes. Esto significa que adaptar un
alfabeto a toki pona solo requiere elegir 14 letras (o secuencias de letras)
que corresponderían al mismo sonido o un sonido similar. Por ejemplo, aquí está
como se convertiría a griego y cirílico.

| Latino | Griego | Cirílico |
|:-----:|:-----:|:--------:|
| a | α | а |
| e | ε | е |
| i | ι | и |
| j | γ | й |
| k | κ | к |
| l | λ | л |
| m | μ | м |
| n | ν | н |
| o | ο | о |
| p | π | п |
| s | σ | с |
| t | τ | т |
| u | υ | у |
| w | β | в |

> %note%
> Estos alfabetos no son completamente compatibles con los sonidos del griego y
> ruso. Algunas de las letras del toki pona se pronuncian diferente de sus
> letras correspondientes.

Es un poco más difícil adaptar toki pona a un alfasilabario o alfabeto
consonántico -- un sistema de escritura donde las consonantes tienen letras
específicas, y las vocales son escritas como diacríticos o símbolos adicionales
al costado. El alfabeto consonántico más famoso es el árabe, y el alfasilabario
más famoso es el devanagari (que se usa para varios idiomas en India).

* [Video "ARABI PONA - Arabic Script for Toki Pona | sitelen musi"][arabic]
* ["Toki Pona in Devanāgarī" (archivado)][devanagari]

[arabic]:https://www.youtube.com/watch?v=Mh9Wypm6pXs
[devanagari]:https://web.archive.org/web/20060727115116/http://www.deadlybrain.org/projects/tokipona/deva_guja.php

El idioma también tiene solo 92 sílabas posibles (47 si "-n" se trata como una
sílaba separada). Esto significa que también se puede adaptar a varios sistemas
de escritura silábicos.

Por ejemplo, aquí hay sugerencias de cómo escribir toki pona usando hangul, el
sistema de escritura del coreano. (Aunque incluye símbolos representado sonidos
individuales como un alfabeto, están ordenados en bloques silábicos.)

* ["Writing Toki Pona with Korean Hangul" (archivado)][hangularch]
* ["Hangul for Toki Pona" en Reddit][hangulred]

[hangularch]:https://web.archive.org/web/20070313181500/http://www.tokipona.bravehost.com/korean.html
[hangulred]:https://www.reddit.com/r/tokipona/comments/8mx951/hangul_for_toki_pona/

Con algunos cambios relativamente pequeños en los sonidos, también se puede
escribir con el sistema hiragana del japonés, propuesto aquí:

* ["Hiragana for Toki Pona"][hiragana1]
* ["sitelen Hiragana (ひらがな)" en Reddit][hiragana_red]

[hiragana1]:https://www.deviantart.com/derroflcopter/journal/Hiragana-for-Toki-Pona-339541633
[hiragana_red]:https://www.reddit.com/r/tokipona/comments/e7g91u/sitelen_hiragana_%E3%81%B2%E3%82%89%E3%81%8C%E3%81%AA/

## sitelen pona

El sistema de escritura creado para toki pona más común es el sistema
logográfico *sitelen pona* ("escritura simple"), creado por Sonja Lang ella
misma y publicado en el libro oficial.

Parecido al alfabeto latino, se escribe de izquierda a derecha y de arriba a
abajo, pero cada carácter representa una palabra entera en vez de un sonido (o
incluso más si se usan caracteres compuestos). Las palabras no oficiales se
escriben dentro de cartuchos (formas largas que rodean un grupo de caracteres),
con caracteres por cada letra añadidos adentro.

* **[Descripción completa, fuentes y ejemplos](es_sitelen_pona.html)**

## sitelen sitelen

["sitelen sitelen"](https://jonathangabel.com/toki-pona/) de Jonathan Gabel
es un sistema de escritura diseñado para ser una manera más estéticamente agradable de escribir textos en toki pona. Es un sistema no lineal inspirado visualmente por la escritura maya.

Comparado a toki pona escrito en el alfabeto latino o en sitelen pona, sitelen
sitelen es significativamente más difícil de entender, y por lo tanto se usa
raramente por la comunidad. Sin embargo, el estilo visual impresionante de los
textos escritos en éste -- como [este contrato][sitelencon] o los
[proverbios][sitelenprov] en toki pona -- varios de los cuales se encuentran en
el libro oficial --  no se puede negar.

[sitelencon]: https://www.jonathangabel.com/archive/2012/artworks_lipu-lawa-pi-esun-kama.html
[sitelenprov]: https://jonathangabel.com/toki-pona/dictionaries/gallery/

## sitelen telo

El sistema ["sitelen
telo"](https://twitter.com/aarontoponce/status/1316350094598459392?lang=en) es
otro de los sistemas de escritura para toki pona creados por la comunidad.
Deriva su estilo de los kana y kanji usados para el japonés, con caracteres
escritos con líneas rectas y formas curvas suaves. Consiste de "linja telo",
un sistema logográfico donde todas las palabras oficiales (y algunas populares
creadas por la comunidad) tienen un carácter, y de "linja sin", un sistema
silabario parecido al hangul para palabras no oficiales.

![tabla de caracteres de sitelen telo](/tokipona/sitelen_telo.gif)
![tabla de caracteres de sílaba de linja sin](/tokipona/sitelen_telo_2.gif)

[Página principal](index.html)

