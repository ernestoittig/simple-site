% linux framebuffer palette switching
% /dev/urandom
% 2021-05-26

Just like graphical terminals such as `urxvt`, the Linux framebuffer has an
option to change the built-in palette. Distributions like Ubuntu use it to make
the palette look nicer during bootup.

<!-- cut -->

The escape sequence to do this is pretty simple:

`ESC ] P # rrggbb`

(without spaces) where `ESC` is the Escape code, # is the hexadecimal color number (from 0 to F)
and `rr`, `gg` and `bb` are replaced with the hexadecimal RGB values
respectively.

[Here is a simple script](/utils/palette.sh) that should be able to set the
palette to one of the several different options:

* the EGA/VGA default palette, as used by MS-DOS and most BIOSes

* the Tango Desktop Project-based terminal palette

* the Terminal.app palette, as defined by Wikipedia

* the Windows 10 console palette

The program doesn't require root privileges, but it has to be executed in a
framebuffer console directly, as opposed to a program like `screen` or `tmux`.
