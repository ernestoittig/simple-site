% on property (and intellectual property)
% /dev/urandom
% 2021-05-04

So I was thinking: the anarchist view that "property is theft" is definitely at
least somewhat true. After all, if society started with the idea that nothing
belongs to anybody, that everything is in the commons, then for property to
appear, whatever was before it had to be "stolen" from the commons.

And this is just talking about the most graceful possible example. In case of
colonialist expansion, things were often literally stolen, and people literally
enslaved, from whichever lands were colonized.

By that logic, property is quite literally theft. But what about "intellectual
property"? After all, there's no limit on what someone can create, right?

<!-- cut -->

Here I'd probably agree on the most basic idea -- that in most cases,
intellectual property is not created by taking something from the commons -- but
even then, there are enough exceptions and ways for people to abuse the system.

 * The most well-known is companies like Disney taking famous public-domain
   works and creating new copyrighted ones after them. On one hand, the original
   fairy tales and stories didn't disappear anywhere, and are still in the
   public domain, but in many people's imaginations, the most prominent versions
   of them are the Disney-made ones.

 * Another issue is that the legal system itself is often skewed in favor of the
   rich and powerful. A big company with a legal department can easily find a
   way to borrow just enough elements from an independent creator's work
   (whether it be a clothing design, or a dance, or a piece of software) in such
   a way as to avoid violating any laws. But the reverse is much less likely.

 * When it comes to laws like the DMCA, these become even easier to abuse, since
   instead of having every claim pass through the legal system, there's an even
   weaker system in place. On websites such as YouTube, people making videos are
   often in a situation where the website presumes guilt and requires users to
   prove their videos don't contain other people's copyrighted content.
   [Sometimes](https://www.lumendatabase.org/blog_entries/800), fraudulent
   companies outright copy and back-date their content in order to censor news
   articles.

And, of course, intellectual property still perpetuates the same inequalities
between people and countries that currently exist. Wikipedia's [list of 50
highest-grossing movies][moviegross] entirely consists of works made in the U.S.
and Europe. The list of [movies by box-office admissions][movieadmit] also
features films made in China, India and other large countries, but is still very
Eurocentric.

People with more money and ability to make intellectual property will get to
make more of it, and while there's no limit to how much can be created, there's
still a limit to how much anyone can see. (Though IMO this is less of an issue
with intellectual property itself and more with the inequalities of the world
today.)

[moviegross]: https://en.wikipedia.org/wiki/List_of_highest-grossing_films
[movieadmit]: https://en.wikipedia.org/wiki/List_of_films_by_box_office_admissions

This gets even worse when you consider inventions that can outright save human
lives. As of the moment of this writing, the COVID-19 pandemic is hitting India
extremely hard, with daily cases not seen anywhere else. Companies in the U.S.,
the U.K., Europe, Russia and China have all developed vaccines for the virus.
From a public health standpoint, it would make the most sense to allow any
country to produce these vaccines, but patent laws make this impossible, and
requests to waive the patents for the duration of the pandemic have been ignored
by all of these countries, which have instead opted for aid schemes that, while
better than nothing, are still inadequate in fighting the pandemic, given that
achieving herd immunity seems to require between 70 and 90% of the population to
be vaccinated.

---

Having said that, simply calling for abolition of intellectual property, at
least while nothing is being done to address all the issues with regular
property, is likely to do more harm than good. Large companies and organizations
will still have more ability to copy independent creators' works than vice
versa. Secretive software companies can still make it hard to copy or
reimplement their code, while freely copying open-source creations with even
less restrictions than before. And research that previously was publicized
(since a patent typically contains a detailed explanation of the thing being
patented and how it works) will just be contained behind closed doors, and
shared among organizations under tight security controls.


