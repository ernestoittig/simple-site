% irssi tips and tricks
% /dev/urandom
% 2021-04-20

Recently found myself using irssi on a setup that's slightly unusual, so I'm
recording the information I found useful for posterity.

 * To switch windows, use `Alt` + number keys for windows 1-10, or `Alt` + top
   row (`qwertyuiop`) for windows 11-20.

 * `Alt+Left` / `Ctrl+p` and `Alt+Right` / `Ctrl+n` switches to the previous or
   next window respectively. It loops around from first to last or vice versa.

 * The script `revolve` is useful, since it compresses consecutive joins/leaves
   onto a single line. Useful for channels where there's not a lot of activity.

