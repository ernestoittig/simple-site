% my impressions from the streets of rage series
% /dev/urandom
% 2021-04-14

As a kid, I didn't have any 16-bit consoles, and at best, my experience with
them was restricted to playing them for a short while when hanging out with my
family's old friends and their kids. Out of the games I got to experience that
way, I played *Sonic* 1 and 2, *Columns*, *Tom & Jerry: Frantic Antics* and
other games, but not any of the *Streets of Rage* games. Instead, the first time
I discovered this games was when I discovered emulators.

These are my basic impressions of both the original three Genesis/Mega Drive
games and the newer ones that have been released.

# streets of rage 1

For the first SoR game, it seems that the developers decided to make their
answer to *Final Fight* by building on top of their older beat-em-ups like
*Golden Axe*. The end result is a game that sometimes feels a bit clunky, but
still somewhat enjoyable to play, especially with Yuzo Koshiro's music. (The
dude's got range, and I also recommend checking out the music from the Master
System and Game Gear versions of *Sonic 1*.) If the series just stopped on this
game, it would probably not have become such a classic, but luckily, the sequel
delivered everything needed to make this series one of Sega's most popular
16-bit franchises.

# streets of rage 2

The second game, instead of feeling like an adaptation of an older engine,
actually plays like a proper response to *Final Fight* and its console ports.
The game adopted a more similar view and gameplay style, while also adding its
own enhancements. Unlike FF's arcade coin-muncher difficulty, SOR2 felt way more
fair to me. The music is still awesome, with "Dreamer" and "Under Logic" (stage
3 and 4 themes) being my favorites. In my opinion, this is the best game to
introduce someone to the series. Seems like Sega has the same idea, because
their *Sega Genesis / Mega Drive Mini* console only includes SOR2.

# streets of rage 3

I haven't played a lot of SOR3, so I can't really say much, but it seems like
this game's gameplay is way more tough than the previous games (apparently, the
Japanese version is easier) and the music is not as catchy -- it went in a
different direction that might sound good on its own, but doesn't fit the game
as well. There are some additions to the gameplay, with more moves and such,
though. From what I have read, the American/European version was modified a lot,
with the plot changed, the player characters' clothes having their color changed
and some features censored, so if you really want to play this one, I'd probably
advise to get a fan-translated Japanese version?

# emptiness

Apparently, there have been several attempts to continue the series during the
Saturn and Dreamcast years, but none of them have gotten off the ground. One
game that was developed as SOR4 ended up being released as "Fighting Force" on
all platforms of the time _but_ the Saturn, but it apparently got a mediocre
reception.

# streets of rage remake

An unofficial "Streets of Rage Remake" fan game was developed between 2003 and
2011 by a group called Bombergames. As the name says, it's a remake of the first
three games. The player can choose four routes, three of which correspond to the
original games, while the fourth one seems to be more original. The gameplay is
by default based on SOR3 with extra enhancements, but it's possible to customize
it to a great extent. The levels from the first game, which had a different
scale and art style, have been redesigned to more closely follow SOR2 and SOR3's
look. The music is all fan-made remixes of the original games' soundtracks,
though there are also tunes taken from "Super Adventure Island" (another game
that Yuzo Koshiro composed for) and "Super Fantasy Zone" (another classic Sega
game).

Overall, it's a very well-made fangame which I can't help but recommend. No
matter which games you like the most, you'll find something you'll like in this
one.

SEGA has issued a takedown against this fangame (which is at odds with their
otherwise ROM-hack-friendly attitude), but it's still widely available on the
internet.

# streets of rage 4

Seems like the developers at Lizardcube, Dotemu and Guard Crush Games had the
same opinion, because when they got to develop the fourth game (released in
2020), they mostly based its gameplay on 2, rather than 3. Most of SOR3's
changes to the gameplay (like all characters being able to dash and roll, or the
timer for special moves) weren't used. Same goes for the music: while the game
has a "retro soundtrack" option, it only borrows the soundtrack from the first
two games in the series (and also the Master System/Game Gear 8-bit versions) --
though from what I heard, this might be a copyright thing?

But the fourth game introduces its own additions that make the game more
exciting and reduce some of the unfairness. Special moves don't take away your
health completely. Instead, it's "borrowed" and can be regained by hitting
enemies while avoiding damage. 

A very welcome change is that, unlike the first three games, which had no save
or password system and were meant to be completed in a single go, this game
offers in-between-level saves in the story mode and also has a "level select"
mode where each level can be beaten by itself. For me and my sometimes limited
attention span, this is a very welcome change. In general, it seems like this
game has more accessibility features: in the story mode, a player can reduce the
level of challenge at cost of points, the limited continues have been replaced
with being able to restart at the current level freely... There's even a "food"
menu in the options where you can replace the health pickups to a variety of
different foods, in case the defaults (an apple and a cooked chicken) don't fit
your preferences. (I've set mine to an onigiri and a bowl of ramen.)

This game has moved away from the originals' (and the remake's) pixel-art visual
style, instead using cartoon-style graphics for its levels and characters. But
you can still unlock retro characters (with all their limitations taken) by
clearing stages and accumulating points, and retro-style boss battles can be
accessed (hint: it involves attacking the arcade machines in a specific way).

Either way, this is a very good continuation of the series, and a game I
really like playing whenever I have the time. And it seems like the developers
plan on expanding on it as well: just a few days ago (April 8th), they announced
[an additional DLC](https://www.youtube.com/watch?v=4LwwRrI-NY0) with new
stages, music tracks, characters and game modes.
