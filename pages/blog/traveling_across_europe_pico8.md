% "Traveling across Europe" for PICO-8
% /dev/urandom
% 2021-11-29

**Original Patreon post**: <https://www.patreon.com/posts/59268018>

![Title demo screenshot](/images/europe_min_0.png)

During the last couple of months, I've been working on a PICO-8 game. This game
is a port of the Russian-made game "Путешествие по Европе" (Traveling across
Europe) for the Dendy video game console (a bootleg clone of the NES/Famicom).

My version largely follows the structure of the original game, but also adds
more features:
     
 * The map of Europe is a modern one, and goes further north and east than the
   original game.
     
 * The game is in English, rather than Russian.
     
 * Since there are almost twice as many visitable countries in this game
   compared to the original, a password system has been added.

The game is functionally complete, and would have been ready for publishing, if
not for some small bugs and the fact that it currently doesn't fit into PICO-8's
"compressed data" limit. I hope I can release it in time for the [PICO-8
edu-game jam](https://itch.io/jam/pico-8-edu-game-jam). 

![Start game screenshot](/images/europe_min_1.png)
![Quiz screenshot](/images/europe_min_2.png)
![Quiz screenshot](/images/europe_min_3.png)
![Map screenshot](/images/europe_min_4.png)
