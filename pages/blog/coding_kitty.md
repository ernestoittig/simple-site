% "coding kitty" website
% /dev/urandom
% 2021-04-30

Recently saw [this cute little website](https://hostrider.com) that has a vector drawing of a kitty that
typrs on a keyboard when clicked. While it seems that most of its contents is
credited and open-sourced, the music isn't. Apparently, it's a royalty-free tune
called "[KLL Rosy Pink (Hold
Me)](https://wowsound.com/product/kll-rosy-pink-hold-me/)" -- thanks to [this
reddit
comment](https://www.reddit.com/r/InternetIsBeautiful/comments/l51doj/coding_kitty_really/gkt5368/)
for the info.
