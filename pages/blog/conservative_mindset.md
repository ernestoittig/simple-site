% the "conservative mindset" is the worst
% /dev/urandom
% 2021-12-30

In the last few weeks, I saw a few of the "[Innuendo
Studios](https://www.youtube.com/c/InnuendoStudios)" "The Alt-Right Playbook"
videos, and these struck me as interesting because even though these videos are
written to describe American conservatism, alt- and far-rightism, etc., a lot of
what the author says also surprisingly applies to politics outside of the U.S.
as well.

These are the specific videos that talk about features of conservatism I found
to be most interesting:

 * [I Hate Mondays](https://www.youtube.com/watch?v=yts2F44RqFw) talks about the
   social conservative view that the most important reason for a law to exist is
   to judge people for "bad" behavior, and fixing any actual problems is
   unimportant compared to that. (e.g. insisting on abstinence-based education,
   restrictive anti-abortion laws or drug-testing welfare recipients even though
   they're all inefficient at their stated goals, but **because** they punish
   sexually active people or drug users.)

 * [Always a Bigger Fish](https://www.youtube.com/watch?v=agzNANfNlTs) talks
   about the fiscal conservative view that the truest form of society is a
   hierarchy where a few "worthy" people succeed and the rest fail and that any
   attempt at providing welfare or reducing inequality is bad because it might
   put people in the wrong place. (Also that a lot of conservatives have their
   own image of what the hierarchy should look like, and in that case the only
   significant difference between a conservative and a fascist is that a
   conservative might believe straight white men deserve a higher spot than they
   get these days, and that a fascist _explicitly_ believes that a hierarchy
   should have a racial/sexual/etc.  criteria.)

 * [The Origins of Conservatism](https://www.youtube.com/watch?v=E4CI2vk3ugk)
   talks about how those conservative views originally came about during the
   French Revolution as a way to preserve the idea of the natural hierarchy in a
   world where the power of kings and queens was being questioned.

The videos are clearly written from an American point of view: words like
**conservative** or **liberal** are used to describe policies that map more or
less onto the Republican and Democratic parties respectively (whereas in Europe,
the word "liberal" is typically used to refer to economic liberalism, a
free-market set of ideas that would be considered conservative or libertarian in
the States).

Having said that, I do think that a lot the conservative features described in
the videos can apply to people both outside of conservatism and countries
outside of the United States. As a Russian citizen, I see the same "conservative
mindset" being used by a lot of Putin supporters -- whether their ideal
historical period is the Russian Empire or even the Soviet Union.

They might believe that a welfare state is good and necessary, but they'll be
happy to condemn people for "abusing" it or that the millions of Russians
currently in a state of poverty isn't an indication that the welfare is
inadequate, but that improverished Russians are just all too lazy to get a job.

They will call for abolition of capitalism, but defend corrupt officials and
oligarchs whenever they work for the benefit of the current government.

They'll criticize the U.S. for police brutality, but then defend the same cops
who committed it (or hold the same bigoted opinions against many of Russia's
ethnic minorities).

Though it's worth noting that where an American conservative is perfectly
capable of thinking that everything in the U.S. is good whenever a Republican is
occupying the office of the President (as can be seen in [this article from
2020](https://fivethirtyeight.com/features/in-2008-everyone-thought-the-recession-was-bad-but-in-2020-many-americans-views-depend-on-their-party/)
where Republican voters suddenly started thinking the economy is good before
Donald Trump was even inaugurated!), Russian reality for a lot of people is more
depressing for that. Nobody reasonable would pretend that Russian courts are
totally impartial or that there isn't a lot of corruption going on.

There's a famous Russian proverb, "от сумы и от тюрьмы не зарекайся". Means
roughly that "Don't be too self-assured that you'll avoid poverty or prison".
Almost everybody understands that even an innocent person can be imprisoned or
lose everything if a few cops or officials decide to not like their face or
something. Therefore, it would be extremely silly to defend someone being put in
prison in blatant violation of due process or to excuse brutal beatings or
tortures that are being inflicted upon prisoners, right? Not to a Russian
conservative.

> %info%
> By the way, isn't it weird that people who like the USSR and otherwise
> describe themselves as socialists believe in so many conservative ideas that
> they'd be treated as right-wing by everyone else if not for their belief that
> the government should run the economy?

# not just conservatives

And it's also worth noting that just because these ideas are most common among
conservatives, that doesn't mean there are no liberals, or socialists for that
matter, who believe in them as well.

A rather easy example would be all of those centrist Democratic voters in the
U.S. who don't support increasing immigration or raising the minimum wage. They
might personally identify as liberals and despise the Republicans, but a lot of
them still hold at least some of the conservative ideas.

A more controversial example would be the Soviet-style authoritarian socialists.
While establishing a state-run socialist economy seems wildly out-of-touch with
other conservative ideas, it's worth remembering that Vladimir Lenin's idea of
socialism frequently mentioned the phrase "Кто не работает, тот не ест" ("One
who doesn't work shall not eat"), as well as the Soviet Union's conservative
positions on social issues such as homosexuality (or the modern perception among
some in the far-left that LGBT rights are "a distraction" from class struggle).

The way I see it, a good way to tell apart people with actual liberal views
from those who just like the liberal party the most is to ask the following
questions:

 * Do they actually believe in the rule of law, or are they just annoyed that
   the wrong people get away with crimes scot-free? Do they criticize
   politicians from their own party when they engage in unethical behaviors?
   When they believe an opponent of theirs deserves to be in prison, is this
   based on actual illegal behavior they have committed?

 * Do they believe that laws which fail to achieve their stated purpose, but
   which hurt a specific group of people in the process, should be abolished or
   kept?

 * Do they sincerely believe that every citizen, no matter their gender, race,
   education, job status, etc. is eligible for the basic rights and freedoms? Do
   they believe that systems designed to benefit a certain disadvantaged class
   (e.g. welfare for poor people or gender self-ID for trans people) being
   hypothetically "abused" by people outside of that group is not
   enough of a reason to eliminate them for everyone else?
