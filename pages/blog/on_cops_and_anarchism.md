% on cops and anarchism
% /dev/urandom
% 2021-06-23

Yet another Twitter/Mastodon thread that grew into a
more verbosely explained blog post.

While I don't consider myself an anarchist, I find a lot of their positions
interesting and I've had a largely good impression of anarchists as a community
online. This blog post is a reflection on the role of policing, as informed by
my view of the anarchist community.

<!-- cut -->

* [Original Twitter thread][twtthread]
* [Original Mastodon thread][mastothread]

[twtthread]: https://twitter.com/_dev_urandom_/status/1407591972949446656
[mastothread]: https://cybre.space/@devurandom/106458718547783437

Whether one lives in a democratic or authoritarian state, the basic impression
any government wants to give to its people is that "this society has laws, you
better follow them or else". The
basic stated job of any police force or legal system is to enforce the laws
passed by the government. Sometimes, these laws make sense and are
unobjectionable by most (hardly any people would justify murder or rape),
sometimes they serve to set more-or-less objective boundaries on what is or
isn't allowed (stealing can be justified in some circumstances, but actually
legislating these circumstances and figuring out when they would apply without
creating gigantic loopholes would be really hard), and sometimes they just serve
to maintain a feeling of public order (like laws against
loitering or vandalism).

But it's hard to convince people that the latter categories are particularly
worth enforcing -- there are some fun-haters who would stand by anti-loitering
laws or claim tagged-up walls are more annoying than the constant streams of
advertising seen pretty much everywhere these days -- so when it's time to
justify police (mis)behavior, typically it's their job at stopping
objectively-evil crimes that's emphasized the most. Even though, statistically,
law enforcement [does a terrible job][2pct_crimes] at it. And even that job
then comes at the expense of cops also arresting, assaulting or killing innocent
people -- or killing people whose crimes would never justify a death penalty in
a court of law. And whenever such an atrocity happens, the friendly relationship
between law enforcement officers and prosecutors, the power of police unions and
other factors make it very unlikely that the police officer would get an
appropriate punishment. Same inefficiency also spreads to property crimes -- the
one area where, in a capitalist society where property rights are supposed to
reign supreme -- law enforcement should be especially efficient.

[2pct_crimes]:https://www.msn.com/en-us/news/crime/police-solve-just-2percent-of-all-major-crimes/ar-BB18bP4T

(And, of course, I haven't yet mentioned race or nationality or religion,
characteristics that make law enforcement's relationship with some groups of
people even worse in a lot of countries.)

However, the one job cops _are_ good at -- and where the more seemingly-useless
laws start to make sense: giving people **the impression** that laws are being
enforced, that there's a certain way things "should" be: person X "should" own
property Y, empty homes "should" not have homeless people in them, public parks
"should" not have skateboarding teenagers. As agents of law and order, police
serves to remind people that there _is_ an order, as written in laws and
property deeds and all other regulatory documents.

Which also is often not true: if you and/or your lawyers can craft good enough
paperwork and outsmart the bureaucrats, or if you appeal to the authorities well
enough, or if you outright bribe people, you can easily rewrite the order of
things and get away with it. The government can write laws to restrict its
abilities, but it can ignore these laws just as easily. On the other hand, a
poor person's access to the legal system is severely hampered. Public defenders
are so understaffed and overworked, that they often ask people to plead guilty to
crimes they have not committed. If they don't have enough time on a case where
they could literally save a person from prison, they'd have even less time for a
property dispute, especially if the other side can drown them in arguments and
documents.

And to me, honestly, the idea that a rich and powerful person can write enough
documents, spend enough money and outsmart enough bureaucrats to start legally
owning something they shouldn't, to the point that a hundred years later, people
won't even remember that the thing in question was basically stolen -- that
sounds less like a 21st century civilized society and more like a medieval grand
strategy game. And, apart from the weapons of war being loopholes, bribes and
clever lawyering instead of swords, spears and catapults -- how _is_ that
different from feudal lords waging wars against each other to conquer others'
lands or put a peasant community under their rule? How is that different from
the stereotypical portrayal of "anarchy" where the strong rule over everyone and
the weak have no protection?

By comparison, _actual_ anarchism at least understands that (1) hierarchical
relations, including ones based on private property, are bad and should be
avoided, and (2) the best way to survive and thrive in a society where you're
not protected from above is to help each other. Anarchists may be stereotyped as
molotov-cocktail-throwing terrorists who want to destroy government so they can
spread chaos, but it seems like in reality, most of them prefer to set up mutual
aid facilities and stuff.

I still find anarchism kinda scary: the idea that there's no book or document or
even web page that says what **should** be allowed or who **should** have the
final say regarding things seems weird to me. It sounds like the kind of society
where you might be assaulted or kicked out of your home simply because your
neighbors don't like your face. But then, that's already reality for a lot of
the world's poor and marginalized people in countries that **do** have law
enforcement systems and books full of seemingly-equal laws that somehow get
applied against the rich and powerful way less often than they should be. And,
even though I'm a middle-class white majority-ethnicity person living in a
country that at least pretends to be a democratic state, if I ever end up in a
situation where justice is violated, I doubt the police will help me, either.
