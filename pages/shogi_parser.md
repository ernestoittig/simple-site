% shogi move parser
% /dev/urandom
% 2022-01-27

I've decided to write a little script using JS that reads a [Japanese notation
for a move in
shogi](https://en.wikipedia.org/wiki/Shogi_notation#Japanese_notation) and tries
to convert it into English.

<script type="text/javascript">

const playerMap = new Map([
  ['▲','black'], //aka "sente" (moves first)
  ['☗','black'],
  ['△','white'], //aka "gote" (moves second)
  ['☖','white'],
]);

//this is just the number map from 10ten's number-parsing code, but without
//zeroes or numbers larger than 9
const digitMap = new Map([
  ['一', 1],
  ['二', 2],
  ['三', 3],
  ['四', 4],
  ['五', 5],
  ['六', 6],
  ['七', 7],
  ['八', 8],
  ['九', 9],
  ['１', 1],
  ['２', 2],
  ['３', 3],
  ['４', 4],
  ['５', 5],
  ['６', 6],
  ['７', 7],
  ['８', 8],
  ['９', 9],
  ['1', 1],
  ['2', 2],
  ['3', 3],
  ['4', 4],
  ['5', 5],
  ['6', 6],
  ['7', 7],
  ['8', 8],
  ['9', 9],
]);

const rankMap = new Map([
  [1,'a'],
  [2,'b'],
  [3,'c'],
  [4,'d'],
  [5,'e'],
  [6,'f'],
  [7,'g'],
  [8,'h'],
  [9,'i'],
]);

const pieceMap = new Map([
  ['歩','pawn'],
  ['桂','knight'],
  ['香','lance'],
  ['銀','silver general'],
  ['金','gold general'],
  ['角','bishop'],
  ['飛','rook'],
  ['王','king'], //typically given to elder or more experienced player
  ['玉','king'], //typically given to younger or less experienced player
  ['と','promoted pawn'],
  ['成桂','promoted knight'],
  ['成香','promoted lance'],
  ['成銀','promoted silver general'],
  ['馬','horse (promoted bishop)'],
  ['龍','dragon (promoted rook)'],
  ['竜','dragon (promoted rook)']
]);

const movementMap = new Map([
  ['打','dropped at'],
  ['引','moves down to'],
  ['寄','moves horizontally to'],
  ['上','moves up to'],
  ['右','moves from the right to'],
  ['左','moves from the left to'],
  ['直','moves vertically to'],
  ['行','moves up to'],
  ['入','moves up to'],
]);

const promoteMap = new Map([
  ['成','and promotes'],
  ['不成','without promoting'],
  ['生','without promoting']
]);

const shogiRegex = /([▲△☗☖]?)([1-9１-９一二三四五六七八九][一二三四五六七八九]|同)(歩|香|桂|銀|金|角|飛|玉|と|成香|成桂|成銀|馬|龍)([打引寄上右左直行入]?)(成|不成|生)?/;
//group 1 (optional): which player gets to move
//group 2: position digits or "same position as last move"
//group 3: piece that moves
//group 4 (optional): disambiguation for the move
//group 5 (optional): whether a piece promotes or not

function parseShogi(text) {
  let shogi = text.match(shogiRegex);
  if (!shogi) return null;
  let res = [];
  let player = playerMap.get(shogi[1]);
  if (player) res.push(player);
  let piece = pieceMap.get(shogi[3]);
  res.push(piece);
  let movement = movementMap.get(shogi[4]);
  if (movement) { res.push(movement); } else { res.push("to"); }
  let coordinates = shogi[2]
  if (coordinates == '同') {
    res.push("previous move's position");
  } else {
    let column = digitMap.get(coordinates[0]);
    let row = rankMap.get(digitMap.get(coordinates[1]));
    if ((!column) || (!row)) return("ERROR: unable to parse \"" + coordinates + "\" as a set of coordinates");
    res.push(column + row);
    //need to parse the coordinates
  }
  let promote = promoteMap.get(shogi[5]);
  if (promote) { res.push(promote); }
  return res.join(" ");
}

function textChanged() {
  let text = document.getElementById("move-input").value;
  let result = parseShogi(text);
  if (result === null) result = "???";
  document.getElementById("move-output").value = result;
}

</script>

<input id="move-input" type="text" placeholder="☗７六歩" 
 onchange = "textChanged();"
 onkeypress = "this.onchange();"
 onpaste = "this.onchange();"
 oninput = "this.onchange();" 
/>
<br />
<textarea id="move-output" type="text" /></textarea>

