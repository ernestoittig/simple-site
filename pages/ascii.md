% ascii table
% /dev/urandom
% 2021-12-23

<style>
.content table {
  margin: auto;
}
.content table tr td:nth-child(3n), .content table tr th:nth-child(3n) {
  border-inline-end: solid;
}
.content table tr td:nth-child(3n+1), .content table tr td:nth-child(3n+2) {
  font-family: monospace;
  font-size: larger;
}
.content table thead, .content table tbody tr:nth-child(16n) {
  border-block-end: solid;
}
.content table tr td {
  padding-block: 2px;
}
</style>

|hex|dec| control char               |hex|dec|punct|hex|dec|upper|hex|dec|lower|
|:-:|:-:|:---------------------------|:-:|:-:|:----|:-:|:-:|:----|:-:|:-:|:----|
| 00|  0| null (`\\0`)               | 20| 32|space| 40| 64|  @  | 60| 96|  `  |
| 01|  1| start of heading           | 21| 33|  !  | 41| 65|  A  | 61| 97|  a  |
| 02|  2| start of text              | 22| 34|  "  | 42| 66|  B  | 62| 98|  b  |
| 03|  3| end of text                | 23| 35|  #  | 43| 67|  C  | 63| 99|  c  |
| 04|  4| end of transmission (`^D`) | 24| 36|  $  | 44| 68|  D  | 64|100|  d  |
| 05|  5| enquiry                    | 25| 37|  %  | 45| 69|  E  | 65|101|  e  |
| 06|  6| acknowledge                | 26| 38|  &  | 46| 70|  F  | 66|102|  f  |
| 07|  7| bell (`\\a`,`^H`)          | 27| 39|  '  | 47| 71|  G  | 67|103|  g  |
| 08|  8| backspace (`\\b`)          | 28| 40|  (  | 48| 72|  H  | 68|104|  h  |
| 09|  9| tab (`\\t`)                | 29| 41|  )  | 49| 73|  I  | 69|105|  i  |
| 0a| 10| line feed (`\\n`)          | 2a| 42|  *  | 4a| 74|  J  | 6a|106|  j  |
| 0b| 11| vertical tab (`\\v`)       | 2b| 43|  +  | 4b| 75|  K  | 6b|107|  k  |
| 0c| 12| form feed (`\\f`)          | 2c| 44|  ,  | 4c| 76|  L  | 6c|108|  l  |
| 0d| 13| carriage return (`\\r`)    | 2d| 45|  -  | 4d| 77|  M  | 6d|109|  m  |
| 0e| 14| shift out                  | 2e| 46|  .  | 4e| 78|  N  | 6e|110|  n  |
| 0f| 15| shift in                   | 2f| 47|  /  | 4f| 79|  O  | 6f|111|  o  |
| 10| 16| data link escape           | 30| 48|  0  | 50| 80|  P  | 70|112|  p  |
| 11| 17| device control one         | 31| 49|  1  | 51| 81|  Q  | 71|113|  q  |
| 12| 18| device control two         | 32| 50|  2  | 52| 82|  R  | 72|114|  r  |
| 13| 19| device control three       | 33| 51|  3  | 53| 83|  S  | 73|115|  s  |
| 14| 20| device control four        | 34| 52|  4  | 54| 84|  T  | 74|116|  t  |
| 15| 21| negative acknowledge       | 35| 53|  5  | 55| 85|  U  | 75|117|  u  |
| 16| 22| synchronous idle           | 36| 54|  6  | 56| 86|  V  | 76|118|  v  |
| 17| 23| end of transmission block  | 37| 55|  7  | 57| 87|  W  | 77|119|  w  |
| 18| 24| cancel                     | 38| 56|  8  | 58| 88|  X  | 78|120|  x  |
| 19| 25| end of medium              | 39| 57|  9  | 59| 89|  Y  | 79|121|  y  |
| 1a| 26| substitute                 | 3a| 58|  :  | 5a| 90|  Z  | 7a|122|  z  |
| 1b| 27| escape                     | 3b| 59|  ;  | 5b| 91|  [  | 7b|123|  {  |
| 1c| 28| file separator             | 3c| 60|  <  | 5c| 92|  \\ | 7c|124|  |  |
| 1d| 29| group separator            | 3d| 61|  =  | 5d| 93|  ]  | 7d|125|  }  |
| 1e| 30| record separator           | 3e| 62|  >  | 5e| 94|  ^  | 7e|126|  ~  |
| 1f| 31| unit separator             | 3f| 63|  ?  | 5f| 95|  _  | 7f|127|`DEL`|

