% conflicting political issues i've yet to resolve
% /dev/urandom
% 2021-08-11

There are some issues where I don't have a single preferred position, because
the different choices all have their own benefits and drawbacks.

# on psychological manipulation, rational choice and democracy

A vital part of any democratic government is that people's right to vote is not
revokable: the government can't suddenly decide that the people "chose wrong"
and break whichever electoral rules it has.

> %warning%
> Which, by the way, is another reason why the Electoral College in the U.S. is
> absolutely awful. Not only it gives extra power to small rural states (and,
> before slavery was abolished, allocated voting power based on population
> counts that included slaves even though slaves couldn't vote!), it also
> intentionally provided a mechanism for electors to outright reject the
> people's decision.

In some way, this resembles the "rational choice theory" of economics: that
people, empowered by their self-interest, will make choices that overall benefit
the economy (or the country, in this case).

Of course, the problem with that theory is that it's often failed in practice.
People are swindled by scammers, motivated by advertisers and marketers to buy
less efficient products, and influenced by the media to adopt different ideas.
If people did actually make rational choices, there would be no such thing as
false advertising or psychologically manipulative patterns.

And politically, governments often _do_ implement laws to restrict what are
otherwise voluntary behaviors, if they can cause problems. We know smoking is
addictive and bad for both the user's health and for those around them, so we
ban indoor smoking and restrict purchases of cigarettes to people over 18 years
old. We know gambling is psychologically addictive and can make people lose
their money and go into debt (a very irrational decision), so we have laws
restricting where casinos can go and banning online gambling.

But then, if we admit that people can make bad decisions, what's preventing a
government from claiming that people who voted for a specific politician or
party from wanting to override their choice based on the same justifications?

If right-wing pundits strongly believe that the "fake news media" is telling
lies about their politicians and parties, or left-wingers insist the media is
"manufacturing consent" for certain policies (with the implication that
otherwise, such consent wouldn't exist), this could easily also be used as a
justification to reject democratic choices.
