% my political positions
% /dev/urandom
% february 2021

On some issues, I hold very strong positions, while on others, I'm open to a lot
of options.

* **the economy**: the end goal is to have everyone's basic needs met in a way
that doesn't hurt future generations. As long as that's met, the details don't
really matter?

* **civil rights**: ethnic, religious, sexual/gender minorities, disabled people
etc. should have the same rights as the rest of society. Discrimination against
these groups can not be tolerated. If a minority group ends up getting less
opportunities because of historic economic factors or discrimination, this
should be rectified. (Of course, groups such as white supremacists who claim
they're being discriminated against because of "white genocide" or religious
fundamentalists who think enforcing their worldview on others is their right are
not included in this category and do not deserve protection.)

* **church and state**: should be separated. In addition to that, existing laws
that aren't explicitly religion-based should be looked at in an effort to make
them more neutral (e.g. if there are official holidays based on the Christmas
season or Easter, these should ideally be made more flexible so that people who
follow other religions, or no religion, are free to pick other days). People
should be free to practice their religion, as long as it doesn't hurt the rights
and freedoms of other people.

* **labor unions**: are good and should be protected, except for jobs that
involve intentionally causing harm to other people (e.g. law enforcement, prison
guards, etc.)

* **police**: both "abolish the police" and "defund the police" are valid
options. The former would probably involve a vast reshaping of the common
conceptions of property and crime, to an extent where I don't understand the
details, but it seems like in many places it would do more good than bad. In the
latter case, the role of policing should be reduced. Ideally, police and prisons
would only be used against violent crimes as a last resort, in case nonviolent
resolution fails to achieve its goals.

* **drugs**: I don't know what the exact best policy for narcotics would be, but
it would probably involve decriminalization of possession, legal production of
drugs in a way where their quality is verified, and letting people who have
addictions lose them without ruining their lives?

* **free speech**: people should be free to say what they want without fear of
legal repercussions, except for threats to other people's lives, harassment,
statements intended to cause harm against a person or a group of people, etc.
However, individual communities and organizations should be free to moderate
and/or restrict speech as they want. It could possibly be a good idea to make it
so that people who have access to large platforms and have a lot of
supporters/followers are made more responsible for statements that might cause
"stochastic terrorism" acts.

* **LGBT rights**: marriage and other similar rights should not be restricted by
sex or gender. A person should be free to declare their gender identity and be
provided medical and social aid in order to have that identity affirmed.
Personal identity documents (passports, driver's licenses, etc.) should ideally
not have a "gender" field at all, but offering alternative options like "X" in
addition to "M" or "F" is also good.

* **gender and sexuality**: people should be free to define themselves as they
want to. (more specifially, bi/pan and nonbinary lesbians are perfectly good
terms.) all consensual, non-abusive relationships between adults are okay. 

* **intellectual property**: was created for a good reason, but its restrictions
have since grown to an extent that hurts creativity and reinforces existing
inequalities. Copyright's restrictions should be limited to 15 years since
publication, with a single possible renewal to another 15 years depending on
whether the work is still being released and used. Patents should be limited to
between 5 and 15 years, depending on the industry (e.g. software patents should
be limited to 5 years, given the pace of innovation in that field).

* **citizenship**: should be granted to anyone if they're born on the country's
territory, or have lived there for over 5 years. should not be revocable.

* **ideal form of government**: a democracy with citizenship and voting rights
strongly protected and a voting system which represents its citizens fairly and
avoids the "spoiler effect" when picking singular leaders. Decentralized
governance, with only the most basic economic and
civil-rights/anti-discrimination restrictions being imposed on a federal level,
is probably the best. I'm also open to anarchist societies, though I have a lot
of questions about how one would even work in the 21st century.

* **ideal size of nations**: not larger than 100 million people, with sizes
between 10 and 50 million being the best.

* **activism**: peaceful is best, but it's also clear that sometimes, large
populations' valid demands are simply ignored or dismissed. In that case,
property damage, vandalism and other kinds of violent activity can be
justifiable, depending on the situation and the level of opposition from the
government. (specifically, I consider BLM protests in the U.S., Hong Kong
democracy protests and the Euromaidan in Ukraine to all be justified.) While
"looting" is, in a moral vacuum, a negative idea, it's more justifiable if the
society otherwise refuses to even pay attention to the protesters' demands, or
if said society's inequality is built on a history of treating some people and
their property as sacred and others' as free to be taken.

* **Crimea**: is Ukraine. Or, alternatively, should be an independent state run
by Crimean Tatars.

---

[Back to main politics page](index.html)
